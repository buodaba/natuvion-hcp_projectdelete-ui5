sap.ui.define([
	"sap/ui/core/mvc/Controller",
    "sap/m/MessageToast"
], function(
	Controller, MessageToast
) {
	"use strict";

	return Controller.extend("com.natuvion.projectDelete.controller.util.HTTPPrepareAjax", {

        getList: function (that, url, oBusyElement) {

            ///////////////////////////////////////////////////////////
            // Success 
            ///////////////////////////////////////////////////////////
            var fSuccess = function (that, msg, status, jqXHR) {
                console.log(msg.response.resultList)
                var oUserModel = that.getView().getModel("projectModel");
                oUserModel.setProperty("/projects", msg.response.resultList);
                console.log(oUserModel.getProperty("/projects"))


            }
            ///////////////////////////////////////////////////////////
            // Error 
            ///////////////////////////////////////////////////////////
            var fError = function () {
                MessageToast.show("ERROR - HTTPPrepareAjax / getList");
            }

            return sap.ui.controller("com.natuvion.projectDelete.controller.util.HTTPAjax").getHTTP(that, url, fSuccess, fError, oBusyElement);
        },

        updateDeprecatedData: function (that, url, data,oBusyElement) {

            
            var oBody = data;
            ///////////////////////////////////////////////////////////
            // Success 
            ///////////////////////////////////////////////////////////
            var fSuccess = function (that, msg, status, jqXHR) {

                



            }
            ///////////////////////////////////////////////////////////
            // Error 
            ///////////////////////////////////////////////////////////
            var fError = function () {
                MessageToast.show("ERROR - HTTPPrepareAjax / getAnalyse");
            }

            return sap.ui.controller("com.natuvion.projectDelete.controller.util.HTTPAjax").postHTTP(that, url, data, fSuccess, fError, oBusyElement);
        },
	});
});