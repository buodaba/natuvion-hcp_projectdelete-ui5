sap.ui.define([
	"sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
], function(
	Controller,MessageToast
) {
	"use strict";

	return Controller.extend("com.natuvion.projectDelete.controller.util.HTTPAjax", {

        
        /////////////////////////////////////////////////////////////////////
        // HTTPAjax Erklärung
        /////////////////////////////////////////////////////////////////////
        /*
            Diese Klasse ist in zwei unterschiedliche Bereiche aufgeteilt. 
            1. Methoden die die Ajax Calls benötigen, wie abfrage des User Token, oder Length Counter
            2. Die Ajax Calls selber. Dabei gibt es unterschiedliche:
                - getHTTP	==> Normaler GET Aufruf
                - postHTTP	==> Normaler POST Aufruf
                - postDownloadFile	==> Wird verwendet um eine Datei zu downloaden
                - postHTTPFile		==> Wird verwendet um eine Datei an den Server zu senden
            Wie diese Funktionen genau verwendet werden entnehmen Sie bitte dem Beispiel
        */

        /////////////////////////////////////////////////////////////////////
        // Helper Method - 
        /////////////////////////////////////////////////////////////////////
        // Alle Funktionen die keine Ajax Calls sind, aber von diesen aufgerufen werden
        /////////////////////////////////////////////////////////////////////

        //Aufruf für c&p
        //	var sbtoa = sap.ui.controller("com.natuvion.projectDelete.controller.util.HTTPAjax")._createBtoa(that);		 
        //
        // AJAX Header Beispiel für c&p
        //	headers: {
        //		"Authorization": "Bearer " + sbtoa
        //	},
        _createBtoa: function(that) {
            var oKeycloak = that.getKeycloak();
			if (oKeycloak === undefined) {
				// that.navigateTo("Home");
				// location.reload();
				return null;
			}
			return oKeycloak.token;
        },

        // oLengthTemp = Ist das Model mit Pfad welches gezahlt werden soll bsp: oModel.getData().response.historicProcesses;
        // oModel = ist das Model zu dem der Counter hinzugefügt wird
        //
        // Aufruf für c&p
        // var oLengthTemp = oModel.getData().response.historicProcesses;
        // oModel = sap.ui.controller("com.natuvion.projectDelete.controller.util.HTTPAjax").getCountLenght(oLengthTemp,	oModel);		 
        getCountLenght: function(oLengthTemp, oModel) {
            var iCount = 0;
            if (oLengthTemp !== undefined && oLengthTemp !== null && oLengthTemp !== "") {
                iCount = oLengthTemp.length;
                if (isNaN(iCount)) {
                    iCount = 0;
                }
            } else {
                iCount = 0;
            }
            oModel.setData({
                "count": iCount
            }, true);
            return oModel;
        },

        /////////////////////////////////////////////////////////////////////
        // HTTP
        /////////////////////////////////////////////////////////////////////
        // Templates für alle HTTP Calls
        /////////////////////////////////////////////////////////////////////

        // GET HTTP Methode für alle Aufrufe
        // sap.ui.controller("com.natuvion.projectDelete.controller.util.HTTPAjax").getHTTP(that, sUrl, fSuccess, fError, oBusyElement);		 
        getHTTP: function(that, sUrl, fSuccess, fError, oBusyElement) {
            var sbtoa = this._createBtoa(that);
     

            if (sbtoa === undefined) {
                debugger;
            }


            //HTTP request
            return $.ajax({
                type: "get",
                url: sUrl,
                contentType: "application/json",
                dataType: "json",
                cache: false,
                headers: {
                    "Authorization": "Bearer " + sbtoa
                },

                success: function(msg, status, jqXHR) {

                    fSuccess(that, msg, status, jqXHR);

                    if (oBusyElement !== undefined) {
                        oBusyElement.setBusy(false);
                    }
                },
                error: function(e) {
                    if (fError !== undefined) {
                        fError(that, e);
                    }

                    if (oBusyElement !== undefined) {
                        oBusyElement.setBusy(false);
                    }
                    MessageToast.show("ERROR - HTTPAjax / getHTTP / sUrl");
                }
            }); //AJAX

        },

        // POST HTTP Methode für alle Aufrufe
        // sap.ui.controller("com.natuvion.sophia.controller.util.HTTPAjax").postHTTP(that, sUrl, oBody, fSuccess, fError, oBusyElement);				 
        postHTTP: function(that, sUrl, oBody, fSuccess, fError, oBusyElement) {
            var sbtoa = sap.ui.controller("com.natuvion.projectDelete.controller.util.HTTPAjax")._createBtoa(that);

            //HTTP request
            return $.ajax({
                type: "post",
                url: sUrl,
                contentType: "application/json",
                dataType: "json",
                cache: false,
                data: JSON.stringify(oBody),
                headers: {
                    "Authorization": "Bearer " + sbtoa
                },
                success: function(msg, status, jqXHR) {

                    fSuccess(that, msg, status, jqXHR);

                    if (oBusyElement !== undefined) {
                        oBusyElement.setBusy(false);
                    }
                },
                error: function(e) {
                    if (fError !== undefined) {
                        fError(that, e);
                    }

                    if (oBusyElement !== undefined) {
                        oBusyElement.setBusy(false);
                    }
                    MessageToast.show("ERROR - HTTPAjax / postHTTP / sUrl");
                }
            }); //AJAX

        },

        // POST HTTP Methode zum download von Dateien
        // sap.ui.controller("com.natuvion.projectDelete.controller.util.HTTPAjax").postDownloadFile(that, sUrl, oBody, fSuccess, fError, oBusyElement);			 
        postDownloadFile: function(that, sUrl, oBody, fSuccess, fError, oBusyElement) {
            var sbtoa = sap.ui.controller("com.natuvion.projectDelete.controller.util.HTTPAjax")._createBtoa(that);

            //HTTP request
            return $.ajax({
                type: "post",
                url: sUrl,
                contentType: "application/json",
                dataType: "json",
                cache: false,
                data: JSON.stringify(oBody),
                headers: {
                    "Authorization": "Bearer " + sbtoa
                },
                success: function(msg, status, jqXHR) {

                    fSuccess(that, msg, status, jqXHR);

                    if (oBusyElement !== undefined) {
                        oBusyElement.setBusy(false);
                    }
                },
                error: function(e) {
                    if (fError !== undefined) {
                        fError(that, e);
                    }

                    if (oBusyElement !== undefined) {
                        oBusyElement.setBusy(false);
                    }
                    MessageToast.show("ERROR - HTTPAjax / postDownloadFile / sUrl");
                }
            }); //AJAX

        },

        // POST HTTP Methode zum upload von Dateien
        // sap.ui.controller("com.natuvion.projectDelete.controller.util.HTTPAjax").postHTTPFile(that, sUrl, fSuccess, fError, oFile, oBusyElement);				 
        postHTTPFile: function(that, sUrl, fSuccess, fError, oFile, oBusyElement) {
            var sbtoa = sap.ui.controller("com.natuvion.projectDelete.controller.util.HTTPAjax")._createBtoa(that);

            return $.ajax({
                type: "POST",
                url: sUrl,
                async: true,
                cache: false,
                processData: false,
                mimeType: "multipart/form-data",
                contentType: false,
                headers: {
                    "Authorization": "Bearer " + sbtoa
                },
                data: oFile,
                success: function(msg, status, jqXHR) {

                    fSuccess(that, msg, status, jqXHR);

                    if (oBusyElement !== undefined) {
                        oBusyElement.setBusy(false);
                    }
                },
                error: function(e) {
                    if (fError !== undefined) {
                        fError(that, e);
                    }

                    if (oBusyElement !== undefined) {
                        oBusyElement.setBusy(false);
                    }
                    MessageToast.show("ERROR - HTTPAjax / postHTTPFile / sUrl");
                }
            });
        }
	});
});