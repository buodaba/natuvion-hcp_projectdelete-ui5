sap.ui.define([
	"com/natuvion/projectDelete/controller/BaseController"
], function (Controller) {
	"use strict";

	return Controller.extend("com.natuvion.projectDelete.controller.App", {
		onInit: function () {
		
	
			var oParentModel = this.getOwnerComponent()._getPropertiesToPropagate().oModels.projectDeleteModel;
            if (oParentModel) {
                // --- App runs inside other app ---
                this.initEmbeddedApp(oParentModel);
            }
			
		},

		initEmbeddedApp: function(oParentModel){
			// Store parent Model
            this.getOwnerComponent().getModel("parentModel").setData(oParentModel.getData());

			var oBackendConfigModel = this.getView().getModel("backendConfigModel");
            oBackendConfigModel.setProperty("/host", oParentModel.getProperty("/host"));

			
            this.getKeycloakPromise(this).then(() => {
							this.navigateTo("ProjectList")
				var sUrl = oParentModel.getProperty("/host") + "/natdeprecateddata/readNatDeprecateddata"

				var oBusyElement = this.getView();
            	oBusyElement.setBusyIndicatorDelay(1);
            	oBusyElement.setBusy(false);

				sap.ui.controller("com.natuvion.projectDelete.controller.util.HTTPPrepareAjax").getList(this, sUrl, oBusyElement);
            });
		}
	});
});