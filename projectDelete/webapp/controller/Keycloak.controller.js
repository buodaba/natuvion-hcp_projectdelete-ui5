/* global Keycloak:true */
sap.ui.define([
    "sap/ui/core/mvc/Controller"

], function(Controller) {
    "use strict";
    var oKeycloak;

    return Controller.extend("com.natuvion.projectDelete.controller.Keycloak", {
        onInit: function() {

        },

        /* Tries *iMaxTries* times with *iIntervalMS* delay between tries to reach the keycloak server and validate token.
         *  Returns a promise that resolves with the authentication status upon receiving a response from the keycloak server.
         *  If keycloak server can not be reached *iMaxTries* times, the promise is rejected.
         */
        checkSSO: function(oController, oKeycloakConfig) {
            var self = this;
            oKeycloak = Keycloak(oKeycloakConfig);

            var iMaxTries = 5;
            var iIntervalMS = 500;

            function rejectAfterDelay(reason) {
                return new Promise(function(resolve, reject) {
                    setTimeout(reject.bind(null, reason), iIntervalMS);
                });
            }

            function attempt() {
                return oKeycloak.init({
                    onLoad: "check-sso",
                    // prevents noticeable navigation (see https://github.com/keycloak/keycloak-documentation/blob/master/securing_apps/topics/oidc/javascript-adapter.adoc)
                    silentCheckSsoRedirectUri: window.location.origin + '/view/fragments/silent-check-sso.html'
                })
            }

            // build a promise chain of *iMaxTries* attempt + delay calls
            // after keycloak server is reached, p resolves and the remaining chain elements are skipped
            var p = Promise.reject();
            for (var i = 0; i < iMaxTries; i++) {
                p = p.catch(attempt).catch(rejectAfterDelay);
            }

            // check if promise resolved or not
            p.then(bAuthenticated => {
                // keycloak server succesfully reached => init stuff
                if (bAuthenticated) {
                    oController.setKeycloak(oKeycloak);
                    self.setUserModel(oController, oKeycloak);
                    self._scheduleTokenRefresh(oKeycloak);
                }
            }).catch(error => {
                // keycloak server could not be reached
                console.error(error);
            });

            oController.setKeycloakPromise(p);
            return p;
        },

        setUserModel(oController, oKeycloak) {
            var sUser = oKeycloak.idTokenParsed.email;
            var aRoles = oKeycloak.realmAccess.roles;

            var oUserModel = oController.getOwnerComponent().getModel("userModel");
            oUserModel.setProperty("/user", sUser);
            oUserModel.setProperty("/name", oKeycloak.idTokenParsed.name);
            oUserModel.setProperty("/roles", aRoles);
            oUserModel.setProperty("/online", true);
        },

        _resetUserModel: function(oController) {
            var oModel = oController.getOwnerComponent().getModel("userModel");
            oModel.setProperty("/user", "");
            oModel.setProperty("/password", "");
            oModel.setProperty("/access", "");
            oModel.setProperty("/roles", []);
            oModel.setProperty("/online", false);
        },

        _scheduleTokenRefresh(oKeycloak) {
            var that = this;

            if (oKeycloak.authenticated && oKeycloak.idToken) {
                // Refresh token every 4 minutes (4*60000ms=240000ms)
                setInterval(function() {
                    // update token if remaining duration < 5 minutes (300s)
                    oKeycloak.updateToken(300).success(function(refreshed) {
                        if (refreshed) {
                            console.log('Keycloak token succesfully refreshed');
                        } else {
                            console.log('Keycloak token is still valid');
                        }
                    }).error(function() {
                        console.log('Failed to refresh Keycloak token, or the session has expired');
                        that.logout(that);
                    });
                }, 240000);

                oKeycloak.onTokenExpired = function() {
                    oKeycloak.updateToken(300).success(function(refreshed) {
                        if (refreshed) {
                            console.log('Keycloak token succesfully refreshed');
                        } else {
                            console.log('Keycloak token is still valid');
                        }
                    }).error(function() {
                        console.log('Failed to refresh Keycloak token, or the session has expired');
                        that.logout(that);
                    });
                };
            }
        }
    });

});