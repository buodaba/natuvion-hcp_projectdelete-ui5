sap.ui.define([
	"com/natuvion/projectDelete/controller/BaseController"
], function (Controller) {
	"use strict";

	return Controller.extend("com.natuvion.projectDelete.controller.ProjectList", {
		addToDeleteRoutine(oEvent){
			
			var oBusyElement = this.getView();
			oBusyElement.setBusyIndicatorDelay(1);
			oBusyElement.setBusy(false);
			var oParentModel = this.getOwnerComponent().getModel("parentModel");
			var oModelData = oEvent.getSource().getBindingContext("projectModel").getObject();
			var sUrl = oParentModel.getProperty("/host") + "/natdeprecateddata/updateNatDeprecateddata";
			if(oModelData){
				oModelData.canbedeleted = true;
			}
			var object = [oModelData]

			let that = this;
			$.when(
				sap.ui.controller("com.natuvion.projectDelete.controller.util.HTTPPrepareAjax").updateDeprecatedData(this, sUrl, object, oBusyElement)
			).done(function (ajaxMessages) {
				var oProjectModel = that.getOwnerComponent().getModel("projectModel");
				var oProjects = oProjectModel.getProperty("/projects");
				var oUpdateEntry = oProjects.find(x => x.id === oModelData.id);
				if(oUpdateEntry){
					oUpdateEntry = oModelData;
				}
				oProjectModel.setProperty("/projects",oProjects)
			}).fail(function (ajaxInitialTask) {
				that.getView().setBusy(false);
				MessageToast.show("ERROR - Das Projekt konnte nicht gelöscht werden");
			});
	
			
			
		}
	});
});