sap.ui.define([
    "sap/ui/core/mvc/Controller",
], function (
    Controller
) {
    "use strict";
    var oGlobalKeyCloak;
    var oGlobalKeyCloakPromise;

    var KeycloakController = sap.ui.controller("com.natuvion.projectDelete.controller.Keycloak");

    return Controller.extend("com.natuvion.projectDelete.controller.BaseController", {
        //Get the router of the Application
        getRouter: function () {
            return sap.ui.core.UIComponent.getRouterFor(this);
        },

        //this function is called on every page with the "<" Button, except home
        onNavBack: function (oEvent) {
            var oHistory, sPreviousHash;
            oHistory = sap.ui.core.routing.History.getInstance();
            sPreviousHash = oHistory.getPreviousHash();
            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                this.getRouter().navTo("home", {}, true /*no history*/);
            }
        },

        //this function can be called in JavaScript for navigation
        navigateTo: function (sRoute, data) {
            var oRouter = this.getRouter();
            oRouter.navTo(sRoute, data);
        },

        setKeycloak: function (oKeycloak) {
            oGlobalKeyCloak = oKeycloak
        },

        getKeycloak: function () {
            return oGlobalKeyCloak;
        },

        setKeycloakPromise: function (oKeyCloakPromise) {
            oGlobalKeyCloakPromise = oKeyCloakPromise
        },

        getKeycloakPromise: function (oController) {
            if (!oGlobalKeyCloakPromise) {
                var oParentModel = oController.getOwnerComponent()._getPropertiesToPropagate().oModels.projectDeleteModel;
                if (oParentModel) {
                    // Re-Use Keycloak object from parent app
                    var oKeycloak = oParentModel.getProperty("/keycloak");
                    this.setKeycloak(oKeycloak);
                    this.setKeycloakPromise(Promise.resolve());
                    KeycloakController.setUserModel(this, oKeycloak);
                } else {
                    // Init key cloak with stand-alone config
                    var oKeycloakConfig = oController.getOwnerComponent().getModel("backendConfigModel").getProperty("/keycloak");
                    KeycloakController.checkSSO(oController, oKeycloakConfig).then(this.navigateTo("Database"));
                }
            }

            return oGlobalKeyCloakPromise;
        },

    });
});